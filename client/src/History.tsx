import React, {useState} from 'react';
import useLocalStorage from "./hooks";
import Patch from "./Patch";

// History
// Permet de gérer l'historique des patchs
function History() {

  const [history, setHistory] = useState<typeof Patch[]>([]); 
  const [counter, setCounter] = useLocalStorage('counter');

  // Annule le dernier patch
  function undo() {
    console.log("undo"); 
  }

  // Applique le dernier patch
  function redo() {
    console.log("redo"); 
  }

  // Pull + Merge + Push sur le serveur 
  function save() {
    console.log("save"); 
  }

  // Pull + Merge localement
  function load() {
    console.log("load"); 
  }

  return (
    <div className="History">
      <h1> Historique </h1>
      <ol>
        {history.map(patch => (<li>{patch}</li>))}
      </ol>
    </div>
  );
}

export default History;
