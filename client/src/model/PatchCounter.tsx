import Patch from './Patch';

export interface PatchCounter extends Patch {
  timestamp: number;
  mutation: number;
  undo: ((state: number) => number);
  redo: ((state: number) => number);
}

export class PatchCounterIncrement implements PatchCounter {

  timestamp: number;
  mutation: number;
  
  constructor(mutation: number) {
    this.timestamp = Date.now();
    this.mutation = mutation;
  }

  undo(state: number) {
    return state - this.mutation; 
  }

  redo(state: number) {
    return state + this.mutation; 
  }
}
