export default interface Patch {
  timestamp: number;
  mutation: number;
  undo: ((state: any) => any);
  redo: ((state: any) => any);
}
