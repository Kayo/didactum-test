import React from 'react';
import ModelCounter from './ModelCounter';

export const LocalContext = React.createContext<ModelCounter>({
  counter: 0,
  history: [],
  undo: () => {},
  redo: () => {}
});
