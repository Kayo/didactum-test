import Patch from './Patch';

export default interface Model {
  counter: number;
  cursor: number;
  history: Patch[];
  apply: ((patch: Patch) => void);
  remove: (() => void);
  undo: (() => void);
  redo: (() => void);
}
