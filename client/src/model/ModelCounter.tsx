import Model from './Model';
import { PatchCounter } from './PatchCounter';

interface ModelCounter extends Model {
  counter: number;
  cursor: number;
  history: PatchCounter[];
  apply: ((patch: PatchCounter) => void);
  remove: (() => void);
  undo: (() => void);
  redo: (() => void);
}

class ModelCounterIncrement implements ModelCounter {
  
  counter: number;
  cursor: number;
  history: PatchCounter[];

  constructor () {
    this.counter = 0;
    this.cursor = -1;
    this.history = [];
  }

  getState() {
    return {
      'counter': this.counter,
      'cursor': this.cursor,
      'history' : this.history
    }
  }

  // Add patch in history, and apply modification
  apply(patch: PatchCounter) {
    this.history.push(patch);
    this.redo();
  }

  // Remove last patch in history, and undo modification
  remove() {
    if (this.cursor == this.history.length)
      this.undo();
    this.history.pop();
  }


  // Undo modification, and change cursor
  undo() {
    if (this.cursor < 0) 
      return

    this.history[this.cursor].undo(this.counter);
    this.cursor--;
  }

  // Redo modification, and change cursor
  redo() {
    if (this.cursor >= this.history.length)
      return

    this.history[this.cursor].redo(this.counter);
    this.cursor++;
  }
}

export default ModelCounterIncrement;
