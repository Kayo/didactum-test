import React from 'react';
import useLocalStorage from './hooks';
import History from './History';

// Counter
// Permet de gérer le compteur et l'affichage de celui-ci
function Counter() {

  const [counter, setCounter] = useLocalStorage('counter');
  
  // Ajoute une valeur au compteur
  function add(value: number) {
    let v = counter + value;
    setCounter("" + v);
  }

  return (
      <div className="App">
        <div className="Counter">
          <h1> Compteur </h1>
          <p> {counter} </p>
        </div>
        <form>
          <button type="button" onClick={() => add(1)}>+1</button>
          <button type="button" onClick={() => add(3)}>+3</button>
          <button type="button" onClick={() => add(5)}>+5</button>
        </form>
        <History />
      </div>
  );
}

export default Counter;
