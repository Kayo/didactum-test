import React from 'react';

// Patch 
// Correspond à une modification effectué sur les données

function Patch (props: any) {

  const timestamp = Date.now();
  
  // Undo le patch 
  function undo() {
    console.log("undo");
  }

  // Réapplique le patch 
  function redo() {
    console.log("redo");
  }

  return (
    <p>id: {timestamp}<br />
    {props.children}</p>
  );
}

export default Patch;
