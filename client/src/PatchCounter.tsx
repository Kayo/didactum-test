import React from 'react';

// PatchCounter 
// Correspond a un patch appliqué précisément au compteur 

function PatchCounter(value: number) {

  const inc = value;

  return (
    <span>{inc}</span>
  );
}

export default PatchCounter;
