// Inspired by https://www.jmptr.com/blog/improving-the-local-storage-hook/

import {useState, useEffect} from 'react';

function useLocalStorage(key: string) {
  // Initialisation
  const [value, setValue] = useState(() => 
    localStorage.getItem(key));

  // A chaque événement sur la donnée, on modifie la valeur en local 
  useEffect(() => {
    const handler = (e: StorageEvent) => {
      if (e.storageArea === localStorage && e.key === key) {
        setValue(e.newValue);
      }
    };
    window.addEventListener('storage', handler);

    return () => {
      window.removeEventListener('storage', handler);
    };
  });

  // A chaque modif en local, on sauvegarde sur le localStorage
  useEffect(() => {
    localStorage.setItem(key, value!);
  }, [key, value]);

  return [parseInt(value!), setValue] as const;
}

export default useLocalStorage;
